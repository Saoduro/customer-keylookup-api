// OPA request
var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");

http.dataAssertValue = context.getVariable("request.queryparam.idvalue");
http.dataAssertKey = context.getVariable("request.queryparam.idtype");

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var input = {};
input.attributes = attributes;

var opa_request = {};
opa_request.input = input

context.setVariable("opa_request", JSON.stringify(opa_request));
context.setVariable("data.api.path","/system/authorize_cdgid_or_email");