var idType = context.getVariable("request.queryparam.idtype");
var idValue = context.getVariable("request.queryparam.idvalue");
context.setVariable("isValidRequest", true);

var idTypes =  ["EMAIL","CDG_ID","BILLING_ID","CHECK_DIGIT_ACCOUNT_ID","SFDC_ACCOUNT_ID","SFDC_USER_ID","SFDC_CONTACT_ID","MYACCOUNT_ID","GIGYA_ID"];
var regex = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);

if (idType === null || idType === "" || idTypes.indexOf(idType) === -1 ) {
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a valid idtype");
    context.setVariable("paramName", "idtype");
} else if (idValue === null || idValue === "") {
    context.setVariable("isValidRequest", false);
    context.setVariable("errorMessage", "Please provide a valid idvalue");
    context.setVariable("paramName", "idvalue");
    
}
if (idType === "EMAIL") {
    if (!regex.test(idValue)) {
        context.setVariable("isValidRequest", false);
        context.setVariable("errorMessage", "Please provide a valid idvalue");
        context.setVariable("paramName", "idvalue");
    }
}