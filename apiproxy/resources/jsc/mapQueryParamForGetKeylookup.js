var jwtEmail = context.getVariable("emailId");
var email = context.getVariable("request.queryparam.email");
context.setVariable("isValidEmail", true);
var regex = new RegExp(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/);
    if (jwtEmail !== null) {
        context.setVariable("request.queryparam.email", jwtEmail);
    }
    else if(email !== null) {
        if (!regex.test(email)){
            context.setVariable("isValidEmail", false);
        }
    }
var timeStamp = new Date().toISOString();
context.setVariable("timeStamp", timeStamp);