var correlation_Id = context.getVariable("request.header.X-CorrelationId");
var payload = JSON.parse(context.getVariable("request.content"));

if (context.getVariable("request.header.authorization") !== null) {
    
    var email = context.getVariable("EMAIL_ADDRESS");
    
    var counter = 0;
    payload.keyMappings.forEach(function(item) {
        if (item.idtype == 'EMAIL_ADDRESS') {
            item.value = email;
            counter = 1;
        }
    });

    if (counter === 0) {
        payload.keyMappings.push({idtype: "EMAIL_ADDRESS", value: email});
    }
}

if (correlation_Id !== null) {
    payload.correlationId = correlation_Id;
    request.content = JSON.stringify(payload);
} else {
    request.content = JSON.stringify(payload);
}
